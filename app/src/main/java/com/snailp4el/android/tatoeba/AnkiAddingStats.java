package com.snailp4el.android.tatoeba;

public class AnkiAddingStats {
    private boolean canAddToAnki;
    private String massageToUser;

    public AnkiAddingStats(boolean canAddToAnki, String massageToUser) {
        this.canAddToAnki = canAddToAnki;
        this.massageToUser = massageToUser;
    }

    public boolean isCanAddToAnki() {
        return canAddToAnki;
    }

    public String getMassageToUser() {
        return massageToUser;
    }
}
