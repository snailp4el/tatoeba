package com.snailp4el.android.tatoeba;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Locale;

public class FetcherCriterias implements Serializable{

    private static final String SERIALISE_FILE = "fetchCridential";
    private static final String TAG = "FetcherCriterias";
    public static final int MAX_COMPLEXITY = 336863;

    private String translateToLanguage = Locale.getDefault().getLanguage();

    private int seekBarProgress = 1;
    private int beginWtihComplexity = 1;
    private int endWithComplexity = 6737;

    private int PhrasesInTheBlock = 10;

    private boolean AddPhraseToAnki = false;
    private boolean autoYesIfButtonNotPresed = true;

    //visibility
    private boolean showNew = true;
    private boolean understood = false;
    private boolean notUnderstood = false;

    public FetcherCriterias() {
        Log.i(TAG, "argument " + beginWtihComplexity);
        this.beginWtihComplexity = checkBeginWith(beginWtihComplexity);
        System.out.println();
        Log.i(TAG,"now "+this.beginWtihComplexity);
    }

    private int checkBeginWith(int i){
        if (i < 1){
            Log.i(TAG, i + " < 1");
        }
        if (i > MAX_COMPLEXITY){
            Log.i(TAG, i + " > MAX_COMPLEXITY");
        }

        return i;
    }

    public boolean isAutoYesIfButtonNotPresed() {
        return autoYesIfButtonNotPresed;
    }

    public void setAutoYesIfButtonNotPresed(boolean autoYesIfButtonNotPresed) {
        this.autoYesIfButtonNotPresed = autoYesIfButtonNotPresed;
    }

    public int getSeekBarProgress() {
        return seekBarProgress;
    }

    public void setSeekBarProgress(int seekBarProgress) {

        this.seekBarProgress = seekBarProgress;
        int step = MAX_COMPLEXITY/50;
        endWithComplexity = seekBarProgress * step;
        beginWtihComplexity = endWithComplexity - step +1;

    }

    public String getTranslationLanguage() {
        return translateToLanguage;
    }

    public void setTranslateTo(String translateTo) {
        this.translateToLanguage = translateTo;
    }

    public int getEndWithComplexity() {
        return endWithComplexity;
    }

    public void setEndWithComplexity(int endWithComplexity) {
        this.endWithComplexity = endWithComplexity;
    }

    public int getPhrasesInTheBlock() {
        return PhrasesInTheBlock;
    }


    public int getBeginWtihComplexity() {
        return beginWtihComplexity;
    }

    public void setBeginWtihComplexity(int beginWtihComplexity) {
        this.beginWtihComplexity = beginWtihComplexity;
    }

    public boolean isAddPhraseToAnki() {
        return AddPhraseToAnki;
    }

    public void setAddPhraseToAnki(boolean addPhraseToAnki) {
        AddPhraseToAnki = addPhraseToAnki;
    }

    public boolean isShowNew() {
        return showNew;
    }

    public void setShowNew(boolean showNew) {
        this.showNew = showNew;
    }

    public boolean isUnderstood() {
        return understood;
    }

    public void setUnderstood(boolean understood) {
        this.understood = understood;
    }

    public boolean isNotUnderstood() {
        return notUnderstood;
    }

    public void setNotUnderstood(boolean notUnderstood) {
        this.notUnderstood = notUnderstood;
    }



    public static void SEARIALIZE(FetcherCriterias fc, Context context){

        try {

            String filePath = context.getFilesDir().getPath().toString() + "/"+SERIALISE_FILE;

            //File file = new File(filePath);
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(fc);
            out.close();
            fileOut.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "SERIALIZE");
    }

    public static FetcherCriterias DISERIALIZE(Context context){

        Log.i(TAG, "DISERIALIZE");

        FetcherCriterias fc = new FetcherCriterias();

        try {
            String filePath = context.getFilesDir().getPath().toString() + "/"+SERIALISE_FILE;

            FileInputStream fileIn = new FileInputStream(filePath);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            fc = (FetcherCriterias) in.readObject();
            in.close();
            fileIn.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fc;

    }

}
