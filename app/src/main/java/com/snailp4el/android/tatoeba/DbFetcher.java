package com.snailp4el.android.tatoeba;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;


public class DbFetcher {


    public static final int PARTS = 1000; //parts to divide
    private final String TAG = "DbFetcher";
    private Context mContext;
    ExternalDbOpenHelper dbOpenHelper;
    //Cursor mCursor;


    public DbFetcher(Context context) {
        mContext = context;
        dbOpenHelper = new ExternalDbOpenHelper(mContext, DbShema.Phrases.FILE_NAME);


        //test
        FetcherCriterias fc = new FetcherCriterias();
        fc.setNotUnderstood(false);
        fc.setUnderstood(true);
        getPhrasesArrayByCriterials(fc);



    }



    public ArrayList<Phrase> getPhrasesArrayByCriterials(FetcherCriterias fc){

        //select * from phrases where complexity > 1 and known is null or 1 and known ==0 or known ==1 order by complexity limit 200
        String showNew = fc.isShowNew()? " and known is null ":" and known is not null ";
        String showUnderstood = fc.isUnderstood()?" or known == 1 ":"";
        String showNotUderstud = fc.isNotUnderstood()?" or known == 0 ":"";


        String sql = "select * from phrases where complexity > " + fc.getBeginWtihComplexity() +
                showNew +
                showUnderstood +
                showNotUderstud + "  order by complexity" +
                " limit " + fc.getPhrasesInTheBlock();

        Log.i(TAG, sql);
        return getPrhrasesBySQL(sql);
    }


    public Phrase getPhraseByCredentiad(int id){
        String sqlQery = "select * from phrases where _id = "+ id + ";";
        return getPrhrasesBySQL(sqlQery).get(0);
    }


    public ArrayList<Integer> getFullIdRowByCriterials(FetcherCriterias fc){

        //select * from phrases where complexity > 1 and known is null or 1 and known ==0 or known ==1 order by complexity limit 200
        String showNew = fc.isShowNew()? " and (known is null ":" and (known == 555 ";
        String showUnderstood = fc.isUnderstood()?" or known == 1 ":"";
        String showNotUderstud = fc.isNotUnderstood()?" or known == 0 ":"";


        String sql = "select * from phrases where complexity >= " + fc.getBeginWtihComplexity() + " and complexity <= " + fc.getEndWithComplexity() + " "+
                showNew +
                showUnderstood +
                showNotUderstud + " )  order by complexity ";

        Log.i(TAG, sql);

        ArrayList<Integer> arrayList = new ArrayList<>();
        Cursor cursor = dbOpenHelper.openDataBase().rawQuery(sql   , null);

        try {
            while (cursor.moveToNext()) {

                arrayList.add(cursor.getInt(0));

            }
        } finally {
            cursor.close();
        }
        return arrayList;
    }



    //sql query
    // select * from phrases where view_date is null
    public ArrayList<Phrase> getPrhrasesBySQL(String sqlQuery){


        ArrayList<Phrase> phrases = new ArrayList<>();
        Cursor cursor = dbOpenHelper.openDataBase().rawQuery(sqlQuery   , null);

        try {
            while (cursor.moveToNext()) {
                //Log.d(TAG, cursor.getString(0) +"|||"+ cursor.getString(1)+"|||"+ cursor.getString(2)+"|||"+ cursor.getString(3)+"|||"+ cursor.getString(4)+"|||"+ cursor.getString(5));

                Phrase p = new Phrase(cursor.getInt(0));
                if(cursor.getString(1)!= null) p.setPhrase(cursor.getString(1));
                if(cursor.getString(2)!= null) p.setTranslation(cursor.getString(2));
                p.setComplexiti(cursor.getInt(3));

                if(cursor.getInt(4)== 1){
                    p.setHasTraslation(true);
                }else {
                    p.setHasTraslation(false);
                }

                if(cursor.getInt(5)== 1){
                    p.setKnown(true);
                }else {
                    p.setKnown(false);
                }

                p.setmDate(cursor.getLong(6));

                phrases.add(p);

            }


        } finally {
            cursor.close();
        }

/*
        for (Phrase p : phrases){
            Log.d(TAG, "" + p.getId());
        }*/

        return phrases;
    }


    public void SavePhraseInTheBase(Phrase phrase){
        int hasTranslation = phrase.isHasTraslation()? 1:0;
        int known = phrase.isKnown()? 1:0;
        int id = phrase.getId();


        ContentValues cv = new ContentValues();
        cv.put(DbShema.Phrases.Cols.HAS_TRANSLATION, hasTranslation);
        cv.put(DbShema.Phrases.Cols.KNOWN, known);
        cv.put(DbShema.Phrases.Cols.WIEW_DATE, phrase.getDate());
        dbOpenHelper.openDataBase().update(DbShema.Phrases.NAME, cv, "_id="+id, null);



    }


    public static String getPhraseInfo(Phrase p){
        String s = "";
        s = s + "_id = " + p.getId() + "\n";
        s = s + "Phrase = " + p.getPhrase() + "\n";
        s = s + "Translation = " + p.getTranslation() + "\n";
        s = s + "Complexity = " + p.getComplexiti() + "\n";
        s = s + "Known = " +  p.isKnown() + "\n";
        s = s + "HasTraslation = " +  p.isHasTraslation() + "\n";
        s = s + "LastChange = "+ p.getDate() + "\n";
        return s;
    }


}

