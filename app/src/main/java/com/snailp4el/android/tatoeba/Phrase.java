package com.snailp4el.android.tatoeba;
import android.util.Log;

import java.io.Serializable;
import java.util.Date;

public class Phrase implements Serializable{

    private int mId;
    private String mPhrase;
    private String mTranslation;
    private int mComplexiti;
    private boolean mHasTraslation;
    private boolean mKnown;
    private long mDate;
    private byte[] byteSound;

    public byte[] getByteSound() {
        return byteSound;
    }

    public void setByteSound(byte[] byteSound) {
        this.byteSound = byteSound;
    }

    public long getDate() {
        return mDate;
    }

    public void setmDate(long mDate) {
        this.mDate = mDate;
    }


    public Phrase(int mId) {
        this.mId = mId;
    }

    public int getId() {
        return mId;
    }

    public String getPhrase() {
        return mPhrase;
    }

    public void setPhrase(String mPhrase) {
        this.mPhrase = mPhrase;
    }

    public String getTranslation() {
        return mTranslation;
    }

    public void setTranslation(String mTranslation) {
        this.mTranslation = mTranslation;
    }

    public int getComplexiti() {
        return mComplexiti;
    }

    public void setComplexiti(int mComplexiti) {
        this.mComplexiti = mComplexiti;
    }

    public boolean isHasTraslation() {
        return mHasTraslation;
    }

    public void setHasTraslation(boolean mHasTraslation) {
        this.mHasTraslation = mHasTraslation;
    }

    public boolean isKnown() {
        return mKnown;
    }

    public void setKnown(boolean mKnown) {
        this.mKnown = mKnown;
    }
}

