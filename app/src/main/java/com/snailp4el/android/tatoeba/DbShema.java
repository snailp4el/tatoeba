package com.snailp4el.android.tatoeba;

public class DbShema {

    public static final int ROWS_AMOUNT_IN_PHRASES = 336267;

    public static final class Phrases {

        public static final String FILE_NAME = "yourdb.sqlite3";
        public static final String NAME = "phrases";
        public static final class Cols {
            public static final String ID = "_id";
            public static final String EN_PHRASE = "en_phrase";
            public static final String TRANSLATION = "translation";
            public static final String COMPLEXITI = "complexity";
            public static final String HAS_TRANSLATION = "has_translation";
            public static final String KNOWN = "known";
            public static final String WIEW_DATE = "view_date";
        }
    }


}
