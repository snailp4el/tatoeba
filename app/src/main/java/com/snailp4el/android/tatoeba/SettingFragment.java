package com.snailp4el.android.tatoeba;


import android.Manifest;
import android.content.Context;

import android.content.DialogInterface;
import android.content.pm.PackageManager;

import android.databinding.DataBindingUtil;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Toast;
import com.ichi2.anki.FlashCardsContract;
import com.snailp4el.android.tatoeba.databinding.FragmentSettingBinding;
import java.util.Locale;
import java.util.TreeSet;


public class SettingFragment extends Fragment implements IOnBackPressed{

    private boolean myCondition = false;
    private int mSeekBarProgres;
    private static final String TAG = "SettingFragment" ;
    FragmentSettingBinding binding;

    FetcherCriterias fetcherCriterias;
    final int PERMISSION_ALL = 1;

    String laguages[];

    String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            FlashCardsContract.READ_WRITE_PERMISSION
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fetcherCriterias = FetcherCriterias.DISERIALIZE(getContext());


    }


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //View v = inflater.inflate(R.layout.seting_fragment, container, false);
       binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);

        // The request code used in ActivityCompat.requestPermissions()
// and returned in the Activity's onRequestPermissionsResult()



       fillTheView();
        binding.AddPhraseToAnki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "setOnClickListener addToAnki Check Button");
            }
        });
        binding.AddPhraseToAnki.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
                                                                        //isButonChecked
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    if(!hasPermissions(getContext(), PERMISSIONS)){
                        requestPermissions( PERMISSIONS, PERMISSION_ALL);

                    }else {
                        // хардкод если не спрашиваем разрешения значит они есть, тогда проверяем установлена ли анки.
                        AnkiAddingStats ankiAddingStats = checkCanWeAddToAnki(getContext());
                        if(!ankiAddingStats.isCanAddToAnki()){
                            binding.AddPhraseToAnki.setChecked(false);
                            Toast toast = Toast.makeText(getContext(), ankiAddingStats.getMassageToUser(), Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }
                }



            }
        });

        //TODO setDifficulty
        binding.dif.seekBarDifficulty.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int i = progress + 1;

            binding.dif.difficultyUperSeekBar.setText(getResources().getString(R.string.difficulty)+ " " + i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        binding.KnownCheckBox.setOnCheckedChangeListener(criterialListner);
        binding.UnknownCheckBox.setOnCheckedChangeListener(criterialListner);
        binding.ShowNewCheckBox.setOnCheckedChangeListener(criterialListner);

        return binding.getRoot();
    }


    CompoundButton.OnCheckedChangeListener criterialListner = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(!binding.KnownCheckBox.isChecked() && !binding.UnknownCheckBox.isChecked() && !binding.ShowNewCheckBox.isChecked()){
                // error
                binding.UnknownCheckBox.setTextColor(getResources().getColor(R.color.colorError));
                binding.ShowNewCheckBox.setTextColor(getResources().getColor(R.color.colorError));
                binding.KnownCheckBox.setTextColor(getResources().getColor(R.color.colorError));

                Toast toast = Toast.makeText(getContext(), getResources().getText(R.string.no_one_checkbox_is_checked), Toast.LENGTH_LONG);
                toast.show();

                myCondition = true; // no one criterials is checked

            }else {
                // OK
                binding.UnknownCheckBox.setTextColor(getResources().getColor(R.color.colorBlack));
                binding.ShowNewCheckBox.setTextColor(getResources().getColor(R.color.colorBlack));
                binding.KnownCheckBox.setTextColor(getResources().getColor(R.color.colorBlack));

                myCondition = false; // just return critrerials is checked
            }
        }
    };


    @Override
    public boolean onBackPressed() {



        if (myCondition) {
            //error ned check atlast one check button

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.visibility)
                    .setMessage(R.string.no_one_checkbox_is_checked)
                    .setIcon(R.drawable.settings)
                    .setCancelable(false)
                    .setNegativeButton(R.string.page_fragment_check_net_button,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();

            return true;
        } else {
            return false;
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        AnkiAddingStats ankiAddingStats = checkCanWeAddToAnki(getContext());
        if (!ankiAddingStats.isCanAddToAnki()){
            binding.AddPhraseToAnki.setChecked(false);

        }

        Toast toast = Toast.makeText(getContext(), ankiAddingStats.getMassageToUser(), Toast.LENGTH_LONG);
        toast.show();
    }

    public static AnkiAddingStats checkCanWeAddToAnki(Context context){

        String message = "";
        boolean b = true;

        if(!isFilePermissionGranted(context)){
            message = message + context.getString(R.string.do_not_have_file_permission) + " \n";
            b = false;
        }

        if(!isPermissionAnkiGranted(context)){
            message = message + context.getString(R.string.do_not_have_anki_permission) + " \n";
            b = false;
        }

        if (!AnkiDroidHelper.isApiAvailable(context)){
            message = message + context.getString(R.string.instal_anki) + " \n";
            b = false;
        }


        if(message == ""){
            message = context.getString(R.string.import_enabled);
        }

        return new AnkiAddingStats(b, message);

    }


    private boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;

                }
            }
        }
        return true;
    }

    public static boolean isPermissionAnkiGranted(Context context){
        boolean b = false;
        if (!AnkiSender.getInstens(context).getmAnkiDroid().shouldRequestPermission()) {
            b = true;
        }
        return b;
    }

    public static boolean isFilePermissionGranted(Context context){
        boolean b = false;
        if (ContextCompat.checkSelfPermission(context , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            b = true;
        }
        return b;
    }




    private void fillTheView(){
        //binding.AddPhraseToAnki.setChecked(fetcherCriterias.isAddPhraseToAnki());
        fillAddToAnkiCheckButton();
        binding.UnknownCheckBox.setChecked(fetcherCriterias.isNotUnderstood());
        binding.KnownCheckBox.setChecked(fetcherCriterias.isUnderstood());
        binding.ShowNewCheckBox.setChecked(fetcherCriterias.isShowNew());


        binding.dif.seekBarDifficulty.setProgress(fetcherCriterias.getSeekBarProgress()-1);
        binding.dif.difficultyUperSeekBar.setText(getResources().getString(R.string.difficulty)+ " " + fetcherCriterias.getSeekBarProgress());

        //test spiner

        Locale[] l = Locale.getAvailableLocales();
        TreeSet<String> set = new TreeSet();
        for(Locale locale: l){
            set.add(locale.getLanguage());
        }

        //String colors[] = {"Red","Blue","White","Yellow","Black", "Green","Purple","Orange","Grey"};

        laguages = new String[set.size()];

        int currentLantInt = 0;
        String lang = fetcherCriterias.getTranslationLanguage();
        int i = 0;
        for (String s: set){
            laguages[i] = s;
            if (s.equals(lang)){
                currentLantInt = i;
            }
            i++;
        }

        binding.MarkAsYesIfButtonsNotPress.setChecked(fetcherCriterias.isAutoYesIfButtonNotPresed());

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, laguages); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.settingSpiner.setAdapter(spinnerArrayAdapter);
        binding.settingSpiner.setSelection(currentLantInt);

    }

    private void fillAddToAnkiCheckButton(){
        boolean checkBoxIs = false;
        if(checkCanWeAddToAnki(getContext()).isCanAddToAnki()& fetcherCriterias.isAddPhraseToAnki()){
            checkBoxIs = true;
        }

        binding.AddPhraseToAnki.setChecked(checkBoxIs);
    }

    private void fillFetchCriterialByView(){
        //todo добавить сообщения пользователю о неправильных критериях

        //SeekBar difficulty
        int progress = binding.dif.seekBarDifficulty.getProgress()+1;
        //int max = binding.dif.seekBarDifficulty.getMax() + 1;
        //int step = FetcherCriterias.MAX_COMPLEXITY / max;
        //int step = 5;//тест
        //int i = (progress) * step;
        //int ii = i + step;
        fetcherCriterias.setSeekBarProgress(progress);

        //fetcherCriterias.setBeginWtihComplexity(i);
        //fetcherCriterias.setEndWithComplexity(ii);

        fetcherCriterias.setAddPhraseToAnki(binding.AddPhraseToAnki.isChecked());
        fetcherCriterias.setNotUnderstood(binding.UnknownCheckBox.isChecked());
        fetcherCriterias.setUnderstood(binding.KnownCheckBox.isChecked());
        fetcherCriterias.setShowNew(binding.ShowNewCheckBox.isChecked());


        fetcherCriterias.setAutoYesIfButtonNotPresed(binding.MarkAsYesIfButtonsNotPress.isChecked());


        fetcherCriterias.setTranslateTo(binding.settingSpiner.getSelectedItem().toString());
        String ss = binding.settingSpiner.getSelectedItem().toString();

    }



    @Override
    public void onPause() {

        Log.i(TAG, "onPause");
        fillFetchCriterialByView();
        FetcherCriterias.SEARIALIZE(fetcherCriterias, getContext());



        super.onPause();
    }



    private int textToInt (String s, int defaultAmount){
        s = s.replaceAll("[^0-9]", "");
        if(s.isEmpty())return defaultAmount;

        int i = Integer.parseInt(s);

        if(i > 0 & i < DbShema.ROWS_AMOUNT_IN_PHRASES){
            return i;
        }
        else return defaultAmount;
    }



}

