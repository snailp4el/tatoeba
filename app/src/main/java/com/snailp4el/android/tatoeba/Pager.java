package com.snailp4el.android.tatoeba;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Pager extends AppCompatActivity {

    //my
    public static DbFetcher dbFetcher;
    private static int eageCounter = 0;
    private FetcherCriterias fc;
    static final String TAG = "Pager";
    private static boolean deleyForEndArray = false;
    ViewPager pager;
    MyFragmentPagerAdapter pagerAdapter;
    private ArrayList<Integer> fullListId;
    private static IPagerFragmentCalback iPagerFragmentCalback;


    private float drag;
    private boolean alertIsShown;


    public static void setiPagerFragmentCalback(IPagerFragmentCalback iPagerFragmentCalback) {
        Pager.iPagerFragmentCalback = iPagerFragmentCalback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //code
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);

        dbFetcher = new DbFetcher(this);

    }


    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_setting:
                startSetings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void startSetings(){
        eageCounter = 0;
        Log.i(TAG, "setin in menu is starting");
        Intent i = new Intent(this, SettingActivity.class);
        startActivity(i);
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();

        showPreScrean();
        if(fullListId.size() == 0 ){
            showSetinAlertDiolog(getResources().getString(R.string.alert_dialog_empthi_array));
        }
        alertIsShown = false;
    }


    private void showSetinAlertDiolog(String titleMessage){

        alertIsShown = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleMessage)
                .setMessage(R.string.alert_diolog_message)
                .setIcon(R.drawable.settings)
                .setCancelable(false)
                .setNegativeButton(R.string.alert_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                startSetings();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();

    }


    //help to show user next screan posobility
    public void showPreScrean(){

        if(fullListId.size() <= 1){
            return;
        }

        pager.postDelayed(new Runnable() {
            @Override
            public void run() {
                pager.beginFakeDrag();
                drag = 0;
                //density - плотность
                float density = getResources().getDisplayMetrics().density;

                int width = getResources().getDisplayMetrics().widthPixels;

                // 120 is the number of dps you want the page to move in total
                ValueAnimator animator = ValueAnimator.ofFloat(0, width, 0 * density);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float progress = (float) valueAnimator.getAnimatedValue();
                        pager.fakeDragBy(drag - progress);
                        drag = progress;
                    }
                });
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        pager.endFakeDrag();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) { }

                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
                animator.setDuration(2000);
                animator.start();
            }
        }, 500);
    }


    @Override
    protected void onStart() {
        Log.i(TAG, "onStart");

        //todo это срабатывает когда возвращаюсь из настроик при первом запуске запускаетмя во вторую очередь
        getArray();

        // check access import to Ankidroid
        if(fc.isAddPhraseToAnki()){
            AnkiAddingStats ankiAddingStats = SettingFragment.checkCanWeAddToAnki(getApplicationContext());
            if(!ankiAddingStats.isCanAddToAnki()){
                fc.setAddPhraseToAnki(false);
                fc.SEARIALIZE(fc, getApplicationContext());

                Toast toast = Toast.makeText(getApplicationContext(), ankiAddingStats.getMassageToUser(), Toast.LENGTH_LONG);
                toast.show();
            }
        }

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        //todo
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            //start when page is active
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected, position = " + position );
                //////////////////////////////////////////////////////////
                //getSupportFragmentManager().findFragmentByTag(fullListId.get(position)+"");

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                Log.d(TAG, " onPageScrolled position - " + position + " positionOffset - " + positionOffset + "positionOffsetPixels - "+ positionOffsetPixels +" count - " + pager.getCurrentItem());

                //pres yes when swipe
                if (positionOffsetPixels > 1 && fc.isAutoYesIfButtonNotPresed()){
                    iPagerFragmentCalback.PresYes();
                }else {
                    iPagerFragmentCalback.UnpresYes();
                }

                if (alertIsShown){
                    return;
                }

                int i = fullListId.size();
                if(eageCounter == 2) {
                    showSetinAlertDiolog(getResources().getString(R.string.alert_dialog_end_of_array));//start seting if end of arrey reach 3 times
                }
                //if(eageCounter == 1) iPagerFragmentCalback.PresYes();
                //TODO message at the eage od array
                //   end of array
                if (position >= i - 1 && !deleyForEndArray && positionOffset == 0){
                    eageCounter++;
                    deleyForEndArray = true;
                    DeleyForEndArray delay = new DeleyForEndArray();
                    delay.execute();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.i(TAG, "onPageScrollStateChanged "+ state + " position" + pagerAdapter.getCount());
            }

        });




        try{
            pager.setCurrentItem(0);//to page one
        }catch (Exception e){

        }

        pagerAdapter.notifyDataSetChanged();//пересоздать view по новому массиву
        super.onStart();
    }


    //todo
    public void getArray(){
        fc = FetcherCriterias.DISERIALIZE(getApplicationContext());
        fullListId = dbFetcher.getFullIdRowByCriterials(fc);


        //phrase for end off array;
        //fullListId.add(56074);

    }

    /////////////////////////////////////////////////////////////////////
    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
        private static final String TAG = "MyFragmentPagerAdapter";
        
        public ArrayList<Integer> getFullListId() {
            return fullListId;
        }

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            //todo при старте запускатся сначала
            //getArray();
        }

        @Override
        public Fragment getItem(int position) {
            //create page
            return PageFragment.newInstance(fullListId.get(position), dbFetcher.getPhraseByCredentiad(fullListId.get(position)),fc);

        }

        @Override
        public int getCount() {
            return fullListId.size();
        }

        @Override
        public int getItemPosition(Object object) {

            return POSITION_NONE;// Для пересоздания view при возврате с сетенгов
        }



    }

    private class DeleyForEndArray extends AsyncTask<Void, Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    deleyForEndArray = false;
                }
            },2000);
            return null;
        }
    }



}
