package com.snailp4el.android.tatoeba;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.snailp4el.android.tatoeba.databinding.FragmentBinding;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class PageFragment extends Fragment implements IPagerFragmentCalback {

    private boolean mButtonPresed = false;

    FragmentBinding binding;

    private static final String TAG = "PageFragment";
    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    public static final String PHRASE = "phrase";
    public static final String FETCHER_CRITERIALS = "fetcherCriterials";

    int pageNumber;
    int backColor;
    Phrase phrase;
    private FetcherCriterias mFetherCrtiterials;
    private boolean isNotUnderstudClikted = false;
    private boolean isAnkiSaved = false;
    private static MediaPlayer mediaPlayer = new MediaPlayer();

    private int playPresed = 0;

    static PageFragment newInstance(int id, Phrase p, FetcherCriterias fc) {
        Log.i(TAG, "newInstance");
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, id);
        arguments.putSerializable(PHRASE, p);
        arguments.putSerializable(FETCHER_CRITERIALS, fc);
        pageFragment.setArguments(arguments);
        return pageFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
        phrase  = (Phrase) getArguments().getSerializable(PHRASE);
        mFetherCrtiterials = (FetcherCriterias) getArguments().getSerializable(FETCHER_CRITERIALS);



        Log.i(TAG, "phrase is comming " + DbFetcher.getPhraseInfo(phrase));

        DownloadSound ds = new DownloadSound();
        ds.execute();

        DownloadTranslation dt = new DownloadTranslation();
        dt.execute();


        Random rnd = new Random();
        backColor = Color.argb(40, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        //View view = inflater.inflate(R.layout.fragment, null);
        // inflate layout, bind fields and etc


        binding = DataBindingUtil.inflate(inflater, R.layout.fragment, container, false);
        //binding.tvPage.setText(phrase.getPhrase());

        binding.tvPage.setBackgroundColor(backColor);


        binding.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPresed++;
                playSound();
            }

        });

        binding.understand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButtonPresed = true;
                showAnswer(true);
            }
        });

        binding.notUderstand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButtonPresed = true;
                showAnswer(false);
                isNotUnderstudClikted = true;

            }
        });

        //last result to button
/*        if(phrase.getDate()>1){
            buttonWorker(phrase.isKnown());
        }*/

        binding.link.setText("https://tatoeba.org/eng/sentences/show/"+phrase.getId());

        return binding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override //TODO play soun when fragmen appere
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.i(TAG, "setUserVisibleHint");
            Pager.setiPagerFragmentCalback(this);
            playSound();
        }else {
            try{
                if(!mButtonPresed && mFetherCrtiterials.isAutoYesIfButtonNotPresed()){
                    Log.i(TAG, "unwisible " + phrase.getPhrase());
                    showAnswer(true);
                }
            }catch (Exception e){

            }
        }

        //TODO check and import to anki
        //  стал невидим        еще не сохр.    кнопка нажата хоть раз      кнопка не активна               в настройках
        if (!isVisibleToUser && !isAnkiSaved && isNotUnderstudClikted && !binding.notUderstand.isEnabled() && mFetherCrtiterials.isAddPhraseToAnki()) {



            //chek and save TODO надо проверить наличие аудео и ответа, установлена ли Анки!!!
                AnkiSender.getInstens(getContext()).addCardsToAnkiDroid(phrase, getContext());
                SaveSound saveSound = new SaveSound();
                saveSound.execute();




            Log.i(TAG, "Import to anki  " + phrase.getPhrase());
            isAnkiSaved = true;

            //AnkiSender.getInstens(getContext()).addCardsToAnkiDroid(phrase, getContext(), this.getActivity());

        }

    }





    private void showAnswer(boolean understood){
        Log.i(TAG, "showAnswer");
        //unsorted

        //show the ansver
        binding.enAnswer.setText(phrase.getPhrase());
        binding.translation.setText(phrase.getTranslation());

        buttonWorker(understood);

        //update the phrase
        phrase.setKnown(understood);
        phrase.setmDate(new Date().getTime());

        //save the phrase in the base
        Log.i(TAG, "save phrase " + DbFetcher.getPhraseInfo(phrase));
        Pager.dbFetcher.SavePhraseInTheBase(phrase);

    }

    private void buttonWorker(boolean understood){
        // make oposite button unactive
        binding.notUderstand.setEnabled(understood);
        binding.understand.setEnabled(!understood);
    }

    public void playSound(){
        if (phrase != null) {

            Log.i(TAG, "curent phrase " + DbFetcher.getPhraseInfo(phrase));//temp


            try {
                playMp3(phrase.getByteSound());;
            }catch (Exception e){

                if(playPresed >= 2){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle(R.string.page_fragment_check_net_title)
                            .setMessage(R.string.page_fragment_check_net_message)
                            .setIcon(R.drawable.settings)
                            .setCancelable(false)
                            .setNegativeButton(R.string.page_fragment_check_net_button,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else {
                    Toast toast = Toast.makeText(getContext(), R.string.loading, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

        }
    }




    //TODO: nede to move to other class
    private static void playMp3(byte[] mp3SoundByteArray) {
        try {
            // create temp file that will hold byte array
            File tempMp3 = File.createTempFile("curent phrase ", "mp3");
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(mp3SoundByteArray);
            fos.close();

            // resetting mediaplayer instance to evade problems
            mediaPlayer.reset();

            // In case you run into issues with threading consider new instance like:
            // MediaPlayer mediaPlayer = new MediaPlayer();

            // Tried passing path directly, but kept getting
            // "Prepare failed.: status=0x1"
            // so using file descriptor instead
            FileInputStream fis = new FileInputStream(tempMp3);
            mediaPlayer.setDataSource(fis.getFD());

            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException ex) {
            String s = ex.toString();
            ex.printStackTrace();
        }
    }


    //avto add to anki if yes presed
    @Override
    public void PresYes() {

        if(!mButtonPresed){
            binding.understand.setEnabled(false);
        }
    }

    @Override
    public void UnpresYes() {
        if(!mButtonPresed){
            binding.understand.setEnabled(true);
        }
    }



    private class DownloadSound extends AsyncTask<Void, Void, Void>{
        private static final String TAG = "DownloadSound";

        @Override
        protected Void doInBackground(Void... voids) {
            Log.i(TAG, "doInBackground");

            do{
                try {
                    phrase.setByteSound(TatoebaFetcher.getUrlBytes("https://audio.tatoeba.org/sentences/eng/"+phrase.getId()+".mp3"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }while (phrase.getByteSound()== null);

            return null;
        }
    }

    private class DownloadTranslation extends AsyncTask<Void, Void, Void>{
        private static final String TAG = "DownloadTranslation";

        @Override
        protected Void doInBackground(Void... voids) {




            try {
                String s = "can't get translation";

                //todo///////test TatebaFetcher///////////////
                s = TatoebaFetcher.getPhraseTranslationFromTateba(phrase.getId(), mFetherCrtiterials.getTranslationLanguage());

                Log.i(TAG, "s = " + s );

                if(s.equals("can't get translation")){
                    s = Translator.callUrlAndParseResult("en", mFetherCrtiterials.getTranslationLanguage(), phrase.getPhrase());
                    //s = Translator.callUrlAndParseResult(phrase.getPhrase());
                    Log.i(TAG, "s === " + s );
                }

                phrase.setTranslation(s);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }



    //сохранить аудио файл
    private class SaveSound extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {


            File phraseFile = new File(AnkiSender.getPhrasePath(getActivity()) + "/"+phrase.getId() +".mp3");

            boolean b = FileWorker.SAVE_FILE(phraseFile, phrase);
            return null;
        }
    }




}