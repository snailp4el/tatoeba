package com.snailp4el.android.tatoeba;


import android.app.DownloadManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.ArraySet;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.TreeSet;

import static android.content.Context.DOWNLOAD_SERVICE;

public class TatoebaFetcher {

    public static final String TAG = "TatoebaFetcher";
    private static MediaPlayer mediaPlayer = new MediaPlayer();


    public static String getPhraseTranslationFromTateba(int phraseId, String mLanguageToTranslate){

        String URL = "https://tatoeba.org/eng/sentences/show/"+ phraseId ;
        String translation = "can't get translation";


        try {
            Document doc = Jsoup.connect(URL).get();
            Elements elements = doc.select("div.sentenceContent");

            //elements = elements.select("div[lang="+ FetcherCriterias.getTranslationLanguage() + "]");

            //all language
            //Locale[] ll = Locale.get
            //String lang = Locale.getDefault().getLanguage();
            elements = elements.select("div[lang="+ mLanguageToTranslate + "]");
            if(!elements.isEmpty())translation = (elements.first().text());//translatinon


        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Translation - "+ translation);
        return translation;
    }

    public static String GET_TRANSHALATION (String phrasId ,String langToTranslate){
        String translation = "can't get translation";



        return translation;
    }







    public static byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();


        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }


}
